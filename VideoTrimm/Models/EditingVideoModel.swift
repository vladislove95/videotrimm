//
//  EditingVideoModel.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import Foundation
import AVKit

struct TrimmInfo {
    var startTime: Double
    var endTime: Double
}

class EditingVideoModel: NSObject {
    let originalVideoURL: URL
    let originalVideoDuration: Double
    var trimVideoInfo: TrimmInfo
    
    var compositionAsset: AVAsset?
    
    var exportedVideoURL: URL?
    
    var exportSession: AVAssetExportSession?
    
    init(originalURL: URL, videoDuration: Double, trimmInfo: TrimmInfo) {
        originalVideoURL = originalURL
        originalVideoDuration = videoDuration
        trimVideoInfo = trimmInfo
        super.init()
    }
}
