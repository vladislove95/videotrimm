//
//  EditedVideoViewModel.swift
//  VideoTrimm
//
//  Created by Vlad on 04.04.2021.
//

import Foundation
import AVKit

class EditedVideoViewModel {
    weak var view: EditedVideoController?
    
    let exportedVideoURL: URL
    let originalVideoAsset: AVAsset
    
    init(with exportedVideoURL: URL) {
        self.exportedVideoURL = exportedVideoURL
        self.originalVideoAsset = AVAsset(url: exportedVideoURL)
    }
}

//MARK: -Public methods
extension EditedVideoViewModel {
    func backButtonTrigger() {
        if let controller = view {
            controller.navigationController?.popViewController(animated: true)
        }
    }
    
    func shareTrigger() {
        guard let controller = view else {
            return
        }
        
        var items: [Any] = []
        items.append(exportedVideoURL)
        items.append(String("Check this edited video!"))

        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        
        activityController.excludedActivityTypes = [UIActivity.ActivityType.addToReadingList,
                                          UIActivity.ActivityType.openInIBooks,
                                          UIActivity.ActivityType.print]
        
        controller.navigationController?.present(activityController, animated: true, completion: nil)
    }
}
