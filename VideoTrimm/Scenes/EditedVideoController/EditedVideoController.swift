//
//  EditedVideoController.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import Foundation
import UIKit
import AVKit

class EditedVideoController: ViewController {
    private lazy var videoView: VideoView = {
        let videoView = VideoView()
        return videoView
    }()
    
    private lazy var backButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(backButtonTrigger), for: .touchUpInside)
        button.setImage(UIImage(named: "arrow")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.tintColor = .white
        return button
    }()
    
    private lazy var shareButton: UIButton = {
       let button = UIButton()
        button.addTarget(self, action: #selector(shareButtonTrigger), for: .touchUpInside)
        button.setTitle("Share", for: .normal)
        button.backgroundColor = .blueButton
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 8
        button.layer.masksToBounds = true
        button.contentEdgeInsets = .init(top: 4, left: 8, bottom: 4, right: 8)
        return button
    }()
    
    private let viewModel: EditedVideoViewModel
    
    init(with viewModel: EditedVideoViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("EditedVideoController init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        viewModel.view = self
        
        view.backgroundColor = .black
        
        view.addSubview(videoView)
        view.addSubview(backButton)
        view.addSubview(shareButton)
        
        configureVideoView()
    }
    
    override func initContraints() {
        videoView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [videoView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
             videoView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
             videoView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
             videoView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)])
        
        backButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [backButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
             backButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
             backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor, multiplier: 1),
             backButton.widthAnchor.constraint(equalToConstant: 24)])
        
        shareButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [shareButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16),
             shareButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16)])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        videoView.play()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

private extension EditedVideoController {
    func configureVideoView() {
        videoView.setup(with: viewModel.originalVideoAsset)
        videoView.setVideoLoopMode()
    }
    
    @objc func backButtonTrigger() {
        videoView.pause(showPauseOverlay: false)
        viewModel.backButtonTrigger()
    }
    
    @objc func shareButtonTrigger() {
        videoView.pause(showPauseOverlay: true)
        viewModel.shareTrigger()
    }
}
