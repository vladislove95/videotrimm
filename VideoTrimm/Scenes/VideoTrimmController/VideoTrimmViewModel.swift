//
//  VideoTrimmViewModel.swift
//  VideoTrimm
//
//  Created by Vlad on 04.04.2021.
//

import Foundation
import AVKit

class VideoTrimmViewModel {
    weak var view: VideoTrimmController?
    
    private let composer = VideoComposer()
    
    let originalVideoURL: URL
    let originalVideoAsset: AVAsset
    let originalVideoDuration: Double
    let editingVideoModel: EditingVideoModel
    
    init(with originalVideoURL: URL) {
        self.originalVideoURL = originalVideoURL
        self.originalVideoAsset = AVAsset(url: originalVideoURL)
        self.originalVideoDuration = self.originalVideoAsset.duration.seconds
        
        let trimmVideoInfo = TrimmInfo(startTime: .zero, endTime: originalVideoDuration)
        self.editingVideoModel = EditingVideoModel(originalURL: originalVideoURL,
                                                   videoDuration: originalVideoDuration,
                                                   trimmInfo: trimmVideoInfo)
    }
}

//MARK: -Public methods
extension VideoTrimmViewModel {
    func backButtonTrigger() {
        if let controller = view {
            controller.navigationController?.popViewController(animated: true)
        }
    }
    
    func appendComposition(newComposition: VideoComposition) {
        composer.compositions.append(newComposition)
    }
    
    func appendDeletedTimeRange(timeRange: CMTimeRange) {
        composer.compositions.last?.deletedRanges.append(timeRange)
    }
    
    func didFinishedEditing(with player: AVPlayer) {
        if let compositionCopy = composer.compositions.last?.copy() {
            composer.compositions.last?.deletedRanges.removeAll()
            
            composer.compositions.append(compositionCopy)
            composer.compositions.last?.updatePlayer(player: player)
            
            if let controller = view {
                controller.updateTrimmView(with: compositionCopy)
                controller.playVideo()
            }
        }
    }
    
    func continueTrigger() {
        editingVideoModel.compositionAsset = composer.compositions.last?.playerItem?.asset
        exportHighResVideo { [weak self] (success, exportedVideoURL) in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async {
                guard let controller = self.view else {
                    return
                }
                
                controller.hideShowProgressView(show: false)
                
                if success, let exportedVideoURL = exportedVideoURL {
                    self.openEditedVideoController(with: exportedVideoURL)
                } else {
                    controller.showErrorAlert()
                }
            }
        }
    }
}

//MARK: -Private methods
private extension VideoTrimmViewModel {
    func openEditedVideoController(with exportedVideoURL: URL) {
        if let controller = view {
            let editedVideoViewModel = EditedVideoViewModel(with: exportedVideoURL)
            let editedVideoController = EditedVideoController(with: editedVideoViewModel)
            controller.navigationController?.pushViewController(editedVideoController, animated: true)
        }
    }
    
    func exportHighResVideo(completionHandler: ((_ success: Bool, _ exportedURL: URL?) -> Void)?) {
        if let exportedVideoURL = editingVideoModel.exportedVideoURL {
            completionHandler?(true, exportedVideoURL)
            return
        }
        
        let oasset = editingVideoModel.compositionAsset ?? AVAsset(url: originalVideoURL)
        
        guard let exportSession = AVAssetExportSession(asset: oasset, presetName: AVAssetExportPresetHighestQuality) else {
            completionHandler?(false, nil)
            return
        }
        
        exportSession.outputURL = URL(fileURLWithPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mp4")
        exportSession.outputFileType = .mp4
        editingVideoModel.exportSession = exportSession
        exportSession.exportAsynchronously { [weak self] in
            guard let self = self else {
                return
            }
            self.editingVideoModel.exportSession = nil
            
            if exportSession.status == .completed {
                self.editingVideoModel.exportedVideoURL = exportSession.outputURL
                
                completionHandler?(true, self.editingVideoModel.exportedVideoURL)
            } else if exportSession.status != .cancelled {
                print("Failed to export video with error: \(exportSession.error?.localizedDescription ?? "")")
                completionHandler?(false, nil)
            }
        }
    }
}
