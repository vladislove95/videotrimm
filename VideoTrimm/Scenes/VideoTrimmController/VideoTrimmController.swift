//
//  VideoTrimmController.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import Foundation
import UIKit
import AVKit

class VideoTrimmController: ViewController {
    private lazy var videoView: VideoView = {
        let videoView = VideoView()
        return videoView
    }()
    
    private lazy var gradientView: GradientView = {
        let gradient = GradientView()
        gradient.colors = [UIColor.black.withAlphaComponent(0.4), .clear, .clear, UIColor.black.withAlphaComponent(0.4)]
        gradient.locations = [0, 0.15, 0.85, 1]
        return gradient
    }()
    
    private lazy var backButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(backButtonTrigger), for: .touchUpInside)
        button.setImage(UIImage(named: "arrow")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.tintColor = .white
        return button
    }()
    
    private lazy var timeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.textColor = .white
        return label
    }()
    
    private lazy var trimmView: TrimmVideoView = {
        let view = TrimmVideoView()
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var confirmTrimmChangesButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "checkmark")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.addTarget(self, action: #selector(confirmTrimmChangesTrigger), for: .touchUpInside)
        button.tintColor = .white
        return button
    }()
    
    private lazy var continueButton: UIButton = {
       let button = UIButton()
        button.addTarget(self, action: #selector(continueButtonTrigger), for: .touchUpInside)
        button.setTitle("Continue", for: .normal)
        button.backgroundColor = .blueButton
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 8
        button.layer.masksToBounds = true
        button.contentEdgeInsets = .init(top: 4, left: 8, bottom: 4, right: 8)
        return button
    }()
    
    private lazy var progressView: ExportingProgressView = {
       let progress = ExportingProgressView()
        self.view.addSubview(progress)
        
        progress.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [progress.leadingAnchor.constraint(equalTo: view.leadingAnchor),
             progress.trailingAnchor.constraint(equalTo: view.trailingAnchor),
             progress.topAnchor.constraint(equalTo: view.topAnchor),
             progress.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
        
        return progress
    }()

    private let viewModel: VideoTrimmViewModel
    
    init(with model: VideoTrimmViewModel) {
        self.viewModel = model
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("VideoTrimmController init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        viewModel.view = self
        
        view.backgroundColor = .black
        
        view.addSubview(videoView)
        videoView.addSubview(gradientView)
        
        view.addSubview(backButton)
        view.addSubview(timeLabel)
        view.addSubview(trimmView)
        view.addSubview(continueButton)
        view.addSubview(confirmTrimmChangesButton)
        
        confirmTrimmChangesButton.isHidden = true
    }
    
    override func initContraints() {
        videoView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [videoView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
             videoView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
             videoView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
             videoView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)])
        
        gradientView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [gradientView.leadingAnchor.constraint(equalTo: videoView.leadingAnchor),
             gradientView.trailingAnchor.constraint(equalTo: videoView.trailingAnchor),
             gradientView.bottomAnchor.constraint(equalTo: videoView.bottomAnchor),
             gradientView.topAnchor.constraint(equalTo: videoView.topAnchor)])
        
        backButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [backButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
             backButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
             backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor, multiplier: 1),
             backButton.widthAnchor.constraint(equalToConstant: 24)])
        
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [timeLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
             timeLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
             timeLabel.centerYAnchor.constraint(equalTo: backButton.centerYAnchor)])
        
        trimmView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [trimmView.topAnchor.constraint(equalTo: backButton.bottomAnchor, constant: 16),
             trimmView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
             trimmView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
             trimmView.heightAnchor.constraint(equalToConstant: 80)])
        
        confirmTrimmChangesButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [confirmTrimmChangesButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
             confirmTrimmChangesButton.topAnchor.constraint(equalTo: trimmView.bottomAnchor, constant: 0),
             confirmTrimmChangesButton.heightAnchor.constraint(equalTo: confirmTrimmChangesButton.widthAnchor, multiplier: 1),
             confirmTrimmChangesButton.widthAnchor.constraint(equalToConstant: 32)])
        
        continueButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [continueButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16),
             continueButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16)])
        
        configureVideoView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        videoView.play()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}


//MARK: -Private methods
private extension VideoTrimmController {
    func configureVideoView() {
        videoView.setup(with: viewModel.originalVideoAsset)
        
        trimmView.setup(videoModel: viewModel.editingVideoModel, timeLabel: timeLabel, videoView: videoView)
        trimmView.delegate = self
        
        let composition = VideoComposition()
        if let player = videoView.player {
            composition.setup(player: player)
        }
        
        viewModel.appendComposition(newComposition: composition)

    }

    @objc private func backButtonTrigger() {
        videoView.pause(showPauseOverlay: false)
        viewModel.backButtonTrigger()
    }
    
    @objc private func confirmTrimmChangesTrigger() {
        trimmView.confirmAction()
        confirmTrimmChangesButton.isHidden = true
        continueButton.isEnabled = true
        continueButton.alpha = 1
    }
    
    @objc private func continueButtonTrigger() {
        hideShowProgressView(show: true)
        videoView.pause(showPauseOverlay: true)
        viewModel.continueTrigger()
    }
}

extension VideoTrimmController {
    func hideShowProgressView(show: Bool) {
        progressView.isHidden = !show
    }
    
    func showErrorAlert() {
        let alert = UIAlertController(title: "Error", message: "Failed to export video, sorry", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func playVideo() {
        videoView.play()
    }
    
    func updateTrimmView(with composition: VideoComposition) {
        trimmView.updateFrames(with: composition)
    }
}

//MARK: -TrimmVideoViewDelegate
extension VideoTrimmController: TrimmVideoViewDelegate {
    func didMovedMainBorders(_ trimmVideoView: TrimmVideoView) {
        confirmTrimmChangesButton.isHidden = false
        continueButton.isEnabled = false
        continueButton.alpha = 0.4
    }
    
    func didRemoveTimeRange(_ trimmVideoView: TrimmVideoView, timeRange: CMTimeRange) {
        viewModel.appendDeletedTimeRange(timeRange: timeRange)
    }
    
    func didFinishEditing(_ trimmVideoView: TrimmVideoView, _ withChanges: Bool) {
        if withChanges, let player = videoView.player {
            viewModel.didFinishedEditing(with: player)
        }
    }
}
