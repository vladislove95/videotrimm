//
//  ViewController.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import Foundation
import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        initContraints()
    }
    
    func initialSetup() {
        
    }
    
    func initContraints() {
        
    }
    
    deinit {
        print("\(String(describing: self)) deinited ")
    }
}
