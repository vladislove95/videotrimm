//
//  WelcomeController.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import Foundation
import UIKit

class WelcomeController: ViewController {
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 16
        return stack
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        label.text = "Welcome to video trimmer"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.text = "Select any video from gallery or camera and edit it!"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var selectVideoButton: UIButton = {
        let button = UIButton()
        button.setTitle("Select", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 22, weight: .semibold)
        button.backgroundColor = .blueButton
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(selectButtonTrigger), for: .touchUpInside)
        button.contentEdgeInsets = .init(top: 4, left: 8, bottom: 4, right: 8)
        button.layer.cornerRadius = 8
        button.layer.masksToBounds = true
        return button
    }()
    
    let viewModel: WelcomeViewModel
    
    init(with viewModel: WelcomeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("WelcomeController init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        viewModel.view = self
        
        navigationController?.isNavigationBarHidden = true
        
        view.backgroundColor = .white
        
        view.addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
        view.addSubview(selectVideoButton)
    }
    
    override func initContraints() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24),
             stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24),
             stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor)])
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [titleLabel.leadingAnchor.constraint(equalTo: stackView.leadingAnchor),
             titleLabel.trailingAnchor.constraint(equalTo: stackView.trailingAnchor)])
        
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [descriptionLabel.leadingAnchor.constraint(equalTo: stackView.leadingAnchor),
             descriptionLabel.trailingAnchor.constraint(equalTo: stackView.trailingAnchor)])
        
        selectVideoButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [selectVideoButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
             selectVideoButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -32)])
    }
}

//MARK: -Private methods
private extension WelcomeController {
    @objc func selectButtonTrigger() {
        showVideoSourceAlert()
    }
    
    func openAppSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
        }
    }
}

//MARK: -Alerts
extension WelcomeController {
    func showVideoSourceAlert() {
        let alert = UIAlertController(title: "Choose video source", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { [unowned self] (action) in
            self.viewModel.openVideoPicker(for: .camera)
        }
        
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { [unowned self] (action) in
            self.viewModel.openVideoPicker(for: .gallery)
        }
        
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showDeniedCameraPermissionAlert() {
        let alert = UIAlertController(title: "Failed to open camera due to denied permissions", message: nil, preferredStyle: .actionSheet)
        
        let appSettingsAction = UIAlertAction(title: "Open system settings", style: .default) { [unowned self] (action) in
            openAppSettings()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alert.addAction(appSettingsAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
}
