//
//  WelcomeViewModel.swift
//  VideoTrimm
//
//  Created by Vlad on 04.04.2021.
//

import Foundation

class WelcomeViewModel {
    private lazy var videoPicker: VideoPicker = {
        let picker = VideoPicker()
        picker.delegate = self
        return picker
    }()
    
    weak var view: WelcomeController?
}

//MARK: -Public methods
extension WelcomeViewModel {
    func openVideoPicker(for type: VideoPicker.SourceType) {
        if let controller = view {
            videoPicker.present(for: type, above: controller)
        }
    }
}


//MARK: -Private methods
private extension WelcomeViewModel {
    func openTrimmController(with videoURL: URL) {
        if let controller = view {
            let videoTrimmViewModel = VideoTrimmViewModel(with: videoURL)
            let videoTrimmController = VideoTrimmController(with: videoTrimmViewModel)
            controller.navigationController?.pushViewController(videoTrimmController, animated: true)
        }
    }
}

//MARK: -VideoPickerDelegate
extension WelcomeViewModel: VideoPickerDelegate {
    func didSelect(videoURL: URL) {
        openTrimmController(with: videoURL)
    }
    
    func failedWithDeniedCameraPermission() {
        view?.showDeniedCameraPermissionAlert()
    }
}
