//
//  ExportingProgressView.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import Foundation
import UIKit

class ExportingProgressView: UIView {
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .large)
        return indicator
    }()
        
    init() {
        super.init(frame: .zero)
        initialSetup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("ExportingProgressView init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        if let _ = superview {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
}

private extension ExportingProgressView {
    func initialSetup() {
        backgroundColor = UIColor.black.withAlphaComponent(0.7)
        
        addSubview(activityIndicator)
        
        initConstraints()
    }
    
    func initConstraints() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [activityIndicator.heightAnchor.constraint(equalToConstant: 80),
             activityIndicator.widthAnchor.constraint(equalToConstant: 80),
             activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor),
             activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor)])
    }
}
