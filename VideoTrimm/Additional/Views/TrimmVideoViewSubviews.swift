//
//  TrimmVideoViewSubviews.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import Foundation
import UIKit

//MARK: main borders
class TrimmVideoViewSideBorder: UIView {
    //left of right border
    private var isRightBorder: Bool = false
    //white dot that indicates dragging allowance
    private var dot = UIView()
    //actually it is 3, but we use 1.5 to perfectly fit bounds
    private let lineWidth: CGFloat = 1.5
    //width of rounded border
    private let customWidth: CGFloat = 20
    //indicates start origin X value for handling pan gesture
    public var startPoint: CGFloat = .zero
    //indicates if view is moving (pan gesture)
    public var isMoving = false {
        didSet {
            if !isMoving {
                isForceStopped = false
            }
        }
    }
    //indicates if border movement force stopped (another border, second's limitation, trimm border)
    public var isForceStopped = false {
        didSet {
            if isForceStopped && oldValue != isForceStopped {
                UIFeedbackManager.shared.generateImpact()
            }
        }
    }
    
    init(frame: CGRect, isRight: Bool) {
        super.init(frame: frame)
        self.isRightBorder = isRight
        backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let largerArea = CGRect(
            x: bounds.origin.x + (isRightBorder ? -5 : 5),
            y: bounds.origin.y - 5,
            width: bounds.size.width + 10,
            height: bounds.size.height + 5*2
        )
        return largerArea.contains(point)
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        if let _ = superview {
            drawSubviews()
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        let darkPath = UIBezierPath()
        darkPath.move(to: .zero)
        darkPath.addLine(to: CGPoint(x: rect.size.width, y: 0))
        darkPath.addQuadCurve(to: CGPoint(x: rect.maxX - customWidth, y: 20),
                              controlPoint: CGPoint(x: rect.maxX - customWidth, y: 0))
        
        darkPath.addLine(to: CGPoint(x: rect.maxX - customWidth, y: rect.maxY - 20))
        darkPath.addQuadCurve(to: CGPoint(x: rect.size.width, y: rect.maxY),
                              controlPoint: CGPoint(x: rect.maxX - customWidth, y: rect.maxY))
        
        darkPath.addLine(to: CGPoint(x: .zero, y: rect.maxY))
        darkPath.close()
        
        if isRightBorder {
            darkPath.apply(CGAffineTransform(scaleX: -1, y: 1))
            darkPath.apply(CGAffineTransform(translationX: rect.maxX, y: 0))
        }
        
        darkPath.lineWidth = 0;
        UIColor.black.withAlphaComponent(0.3).setFill()
        darkPath.fill()
        darkPath.stroke()
        
        let linePath = UIBezierPath()
        linePath.move(to: CGPoint(x: rect.maxX, y: rect.minY + lineWidth))
        linePath.addQuadCurve(to: CGPoint(x: rect.maxX - customWidth, y: 20 + lineWidth),
                              controlPoint: CGPoint(x: rect.maxX - customWidth, y: 0 + lineWidth))
        
        linePath.addLine(to: CGPoint(x: rect.maxX - customWidth, y: rect.maxY - 20 - lineWidth))
        linePath.addQuadCurve(to: CGPoint(x: rect.size.width, y: rect.maxY - lineWidth),
                              controlPoint: CGPoint(x: rect.maxX - customWidth, y: rect.maxY - lineWidth))
        
        
        if isRightBorder {
            linePath.apply(CGAffineTransform(scaleX: -1, y: 1))
            linePath.apply(CGAffineTransform(translationX: rect.maxX, y: 0))
        }
        
        linePath.lineWidth = 3
        UIColor.clear.setFill()
        UIColor.white.setStroke()
        linePath.fill()
        linePath.stroke()
    }
    
    private func drawSubviews() {
        dot = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 14, height: 14)))
        dot.backgroundColor = .white
        dot.layer.cornerRadius = 7
        dot.layer.masksToBounds = true
        dot.isUserInteractionEnabled = false
        if isRightBorder {
            dot.center = CGPoint(x: customWidth, y: bounds.size.height / 2)
        } else {
            dot.center = CGPoint(x: bounds.size.width - customWidth, y: bounds.size.height / 2)
        }
        addSubview(dot)
    }
    
    public func hideShowDraggableDot(isDotHidden: Bool) {
        UIView.transition(with: dot, duration: 0.3, options: .curveEaseOut, animations: {
            if isDotHidden {
                self.dot.alpha = 0
                self.dot.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            } else {
                self.dot.alpha = 1
                self.dot.transform = CGAffineTransform.identity
            }
        }, completion: nil)
    }
}

//MARK: slider
class SliderView: UIView {
    //original rect of area between 2 main borders
    var maskRectangle: CGRect = .zero
    //indicates start origin X value for handling pan gesture
    public var startPoint: CGFloat = .zero
    //indicates if view is moving (pan gesture)
    public var isMoving = false
    //larger area for handling pan gesture
    public var largerArea: CGRect {
        return CGRect(
            x: bounds.origin.x - 10,
            y: bounds.origin.y - 5,
            width: bounds.size.width + 10*2,
            height: bounds.size.height + 5*2
        )
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return largerArea.contains(point)
    }
}

//MARK: cut fragment border
class TrimmVideoViewFragmentBorder: UIView {
    //white dot that indicates dragging allowance
    private var dot = UIView()
    //white line - border
    private var whiteLine = UIView()
    //indicates start origin X value for handling pan gesture
    public var startPoint: CGFloat = .zero
    //indicates if view is moving (pan gesture)
    public var isMoving = false {
        didSet {
            updateBorderSelection()
            if !isMoving {
                isForceStopped = false
            }
        }
    }
    //indicates if border movement force stopped (another border, second's limitation, trimm border)
    public var isForceStopped = false {
        didSet {
            if isForceStopped && oldValue != isForceStopped {
                UIFeedbackManager.shared.generateImpact()
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let largerArea = CGRect(
            x: bounds.origin.x - 5,
            y: bounds.origin.y - 5,
            width: bounds.size.width + 5*2,
            height: bounds.size.height + 5*2
        )
        return largerArea.contains(point)
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        if let _ = superview {
            drawSubviews()
        }
    }
    
    private func drawSubviews() {
        clipsToBounds = true
        
        whiteLine = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 3, height: bounds.size.height)))
        whiteLine.backgroundColor = .white
        whiteLine.isUserInteractionEnabled = false
        whiteLine.center = bounds.mid
        addSubview(whiteLine)
        
        dot = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 12, height: 12)))
        dot.backgroundColor = .white
        dot.layer.cornerRadius = 7
        dot.layer.masksToBounds = true
        dot.isUserInteractionEnabled = false
        dot.center = whiteLine.center
        addSubview(dot)
    }
    
    private func updateBorderSelection() {
        UIView.transition(with: dot, duration: 0.15, options: .curveEaseOut, animations: {
            if self.isMoving {
                self.whiteLine.transform = CGAffineTransform(scaleX: 1.5, y: 1)
                self.dot.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            } else {
                self.whiteLine.transform = .identity
                self.dot.transform = .identity
            }
        }, completion: nil)
    }
    
    public func animateDotsAppearance() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.2
        pulse.fromValue = 1.0
        pulse.toValue = 1.5
        pulse.autoreverses = true
        pulse.repeatCount = 1
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        dot.layer.add(pulse, forKey: "pulse")
    }
}
