//
//  VideoView.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import UIKit
import Foundation
import AVKit

class VideoView: UIView {
    private lazy var pauseOverlay: UIView = {
        let overlay = UIView()
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        let playImageView = UIImageView(image: UIImage(systemName: "play")?.withRenderingMode(.alwaysTemplate))
        playImageView.tintColor = .white
        overlay.addSubview(playImageView)
        
        playImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [playImageView.centerYAnchor.constraint(equalTo: overlay.centerYAnchor),
             playImageView.centerXAnchor.constraint(equalTo: overlay.centerXAnchor),
             playImageView.heightAnchor.constraint(equalTo: playImageView.widthAnchor),
             playImageView.heightAnchor.constraint(equalToConstant: 40)])
        
        return overlay
    }()
    
    var player: AVPlayer? {
        get {
            return playerLayer.player
        }
        
        set {
            playerLayer.player = newValue
        }
    }
    
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
    
    var asset: AVAsset? {
        return player?.currentItem?.asset
    }
    
    var playerTimeObserver: Any?
    
    private let output = AVPlayerItemVideoOutput(pixelBufferAttributes: [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA])
    
    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    init() {
        super.init(frame: .zero)
        initialSetup()
    }

    required init?(coder: NSCoder) {
        fatalError("VideoView init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

private extension VideoView {
    func initialSetup() {
        playerLayer.needsDisplayOnBoundsChange = true
        playerLayer.videoGravity = .resizeAspect
        
        addSubview(pauseOverlay)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapTrigger))
        addGestureRecognizer(tapGesture)
        
        hideShowPauseOverlay(show: false)
        
        initContraints()
    }
    
    func initContraints() {
        pauseOverlay.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [pauseOverlay.leadingAnchor.constraint(equalTo: leadingAnchor),
             pauseOverlay.trailingAnchor.constraint(equalTo: trailingAnchor),
             pauseOverlay.topAnchor.constraint(equalTo: topAnchor),
             pauseOverlay.bottomAnchor.constraint(equalTo: bottomAnchor)])
    }
    
    @objc private func viewTapTrigger() {
        playPauseVieo()
    }
    
    func playPauseVieo() {
        if player?.isPlaying == false {
            play()
        } else {
            pause(showPauseOverlay: true)
        }
    }
    
    func hideShowPauseOverlay(show: Bool) {
        UIView.animate(withDuration: 0.25) {
            self.pauseOverlay.isHidden = !show
        }
    }
    
    @objc func playerDidFinishPlaying(notification: Notification) {
        if (notification.object as? AVPlayerItem) == player?.currentItem {
            playerLayer.player?.seek(to: CMTime.zero)
            playerLayer.player?.play()
        }
    }
}

extension VideoView {
    func setup(with asset: AVAsset) {
        player = AVPlayer(playerItem: AVPlayerItem(asset: asset))
        player?.currentItem?.add(output)
        player?.actionAtItemEnd = .none
        player?.isMuted = false
    }
    
    func play() {
        player?.play()
        hideShowPauseOverlay(show: false)
    }
    
    func pause(showPauseOverlay: Bool) {
        player?.pause()
        hideShowPauseOverlay(show: showPauseOverlay)
    }
    
    func setVideoLoopMode() {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: playerLayer.player?.currentItem)
    }
}
