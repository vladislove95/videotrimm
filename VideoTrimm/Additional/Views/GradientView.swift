//
//  GradientView.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import Foundation
import UIKit

class GradientView: UIView {
    public var topColor: UIColor = UIColor.clear {
        didSet {
            gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        }
    }
    
    public var bottomColor: UIColor = UIColor.black.withAlphaComponent(0.35) {
        didSet {
            gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        }
    }
    
    public var locations: [NSNumber]? {
        get {
            return gradientLayer.locations
        }
        set {
            gradientLayer.locations = newValue
        }
    }
    
    public var colors: [UIColor] {
        get {
            return gradientLayer.colors?.compactMap({UIColor(cgColor: $0 as! CGColor)}) ?? []
        }
        set {
            gradientLayer.colors = newValue.map({$0.cgColor})
        }
    }
    
    private lazy var gradientLayer: CAGradientLayer =  {
       let gradient = CAGradientLayer()
        gradient.colors = [topColor.cgColor, bottomColor.cgColor]
        layer.addSublayer(gradient)
        return gradient
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }
}
