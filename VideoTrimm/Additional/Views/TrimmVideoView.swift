//
//  TrimmVideoView.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import UIKit
import AVKit
import Foundation

enum TrimmVideoViewMode {
    case normal
    case moveSideBorders
    case cutFragment
}

protocol TrimmVideoViewDelegate: class {
    func didMovedMainBorders(_ trimmVideoView: TrimmVideoView)
    func didRemoveTimeRange(_ trimmVideoView: TrimmVideoView, timeRange: CMTimeRange)
    func didFinishEditing(_ trimmVideoView: TrimmVideoView, _ withChanges: Bool)
}

class TrimmVideoView: UIView {
    private var scrollView = UIScrollView()
    private var framesView = UIView()
    private let topBorder = UIView()
    private let bottomBorder = UIView()
    private let slider = SliderView()
    private var leftBorder = TrimmVideoViewSideBorder(frame: .zero, isRight: false)
    private var rightBorder = TrimmVideoViewSideBorder(frame: .zero, isRight: true)

    public weak var delegate: TrimmVideoViewDelegate?

    //set here your label to show selected time range
    public weak var timeLabel: UILabel?
    //set here your videoView to update slider position and other actions
    public weak var videoView: VideoView?
    //set here your videoModel to update time ranges
    public weak var videoModel: EditingVideoModel?
    //return current actual asset
    internal var asset: AVAsset? {
        return videoView?.asset
    }
    
    private var imageGenerator: AVAssetImageGenerator?

    //customization
    private let scrollViewFramesInset: CGFloat = 16
    public var maxVideoDuration: Double = 60
    public var framesCountPerMinute: Int = 10
    public var totalFramesCount: Int = 10

    private var secondsPerPoint: Double = .zero
    private var pointsPerSecond: CGFloat = .zero

    public var trimmMode: TrimmVideoViewMode = .normal {
        didSet {
            updateForMode()
        }
    }

    internal var removingFragmentRange: CMTimeRange {
        let startTime = Double(framesView.convert(CGPoint(x: leftFragmentBorder.center.x, y: 0), to: framesView).x - scrollViewFramesInset + scrollView.contentOffset.x) * secondsPerPoint
        let endTime = Double(framesView.convert(CGPoint(x: rightFragmentBorder.center.x, y: 0), to: framesView).x - scrollViewFramesInset + scrollView.contentOffset.x) * secondsPerPoint
        return CMTimeRange(start: CMTime(seconds: startTime, preferredTimescale: 1000),
                           end: CMTime(seconds: endTime, preferredTimescale: 1000))
    }

    private lazy var leftFragmentBorder: TrimmVideoViewFragmentBorder = {
        let borderFrame = CGRect(origin: .zero, size: CGSize(width: 20, height: framesView.frame.height))
        let border = TrimmVideoViewFragmentBorder(frame: borderFrame)
        border.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(leftFragmentBorderMoved(gesture:))))
        border.isHidden = true
        border.alpha = 0
        border.addShadow()
        insertSubview(border, aboveSubview: slider)
        return border
    }()

    private lazy var rightFragmentBorder: TrimmVideoViewFragmentBorder = {
        let borderFrame = CGRect(origin: .zero, size: CGSize(width: 20, height: framesView.frame.height))
        let border = TrimmVideoViewFragmentBorder(frame: borderFrame)
        border.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(rightFragmentBorderMoved(gesture:))))
        border.isHidden = true
        border.alpha = 0
        border.addShadow()
        insertSubview(border, aboveSubview: slider)
        return border
    }()

    private lazy var redDeleteFragment: UIView = {
        let redView = UIView(frame: CGRect(origin: leftFragmentBorder.frame.origin, size: CGSize(width: 0, height: framesView.frame.height)))
        redView.backgroundColor = UIColor.red.withAlphaComponent(0.4)
        redView.isHidden = false
        redView.alpha = 0
        framesView.addSubview(redView)
        return redView
    }()
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        if let _ = superview {
            initScrollView()
            framesView.layer.cornerRadius = 16
            framesView.layer.masksToBounds = true
            framesCountPerMinute = Int(UIScreen.main.bounds.width / 50) //50 - suggested frame width
            scrollView.delegate = self
        }
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        switch trimmMode {
        case .normal, .moveSideBorders:
            if convert(slider.largerArea, from: slider).contains(point) {
                return slider
            }
        case .cutFragment:
            if leftFragmentBorder.frame.contains(point) {
                return leftFragmentBorder
            } else if rightFragmentBorder.frame.contains(point) {
                return rightFragmentBorder
            }
        }
        return super.hitTest(point, with: event)
    }
}

//MARK: ScrollView setup methods
extension TrimmVideoView {
    private func resetFrames() {
        for subview in framesView.subviews {
            (subview as? UIImageView)?.removeFromSuperview()
        }
    }

    private func setupFrames() {
        guard let videoModel = videoModel else {
            return
        }

        let asset = AVAsset(url: videoModel.originalVideoURL)

        let duration = asset.duration.seconds
        imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator?.appliesPreferredTrackTransform = true
        imageGenerator?.maximumSize = CGSize(width: 200, height: 200)
        imageGenerator?.requestedTimeToleranceBefore = CMTime.zero
        imageGenerator?.requestedTimeToleranceAfter = CMTime.zero

        let multiplier = max(1, duration / maxVideoDuration)
        totalFramesCount = Int(Double(framesCountPerMinute) * multiplier)

        framesView.frame = CGRect(x: scrollViewFramesInset, y: 0, width: (bounds.width - scrollViewFramesInset * 2) * CGFloat(multiplier), height: scrollView.bounds.size.height)
        framesView.backgroundColor = .clear

        hideShowDotsOnSides(isDotHidden: duration < 1.05)
        secondsPerPoint = duration / Double(framesView.frame.size.width)
        pointsPerSecond = 1 / CGFloat(secondsPerPoint)

        scrollView.contentSize = CGSize(width: framesView.frame.width + scrollViewFramesInset * 2, height: scrollView.bounds.size.height)
        scrollView.addSubview(framesView)

        DispatchQueue.global(qos: .userInteractive).async {
            for index in 0..<self.totalFramesCount {
                let time = duration / Double(self.totalFramesCount) * Double(index)
                if let image = self.createImage(fromTime: time) {
                    self.addImageToFramesView(image: image, index: index)
                }
            }
        }

    }

    public func updateFrames(with newComposition: VideoComposition) {
        guard let asset = asset else {
            return
        }
        
        resetFrames()

        imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator?.appliesPreferredTrackTransform = true
        imageGenerator?.maximumSize = CGSize(width: 200, height: 200)

        let duration = asset.duration.seconds
        let multiplier = max(1, duration / maxVideoDuration)
        totalFramesCount = Int(Double(framesCountPerMinute) * multiplier)
        framesView.frame = CGRect(x: scrollViewFramesInset, y: 0, width: (bounds.width - scrollViewFramesInset * 2) * CGFloat(multiplier), height: scrollView.bounds.size.height)
        secondsPerPoint = duration / Double(framesView.frame.size.width)
        pointsPerSecond = 1 / CGFloat(secondsPerPoint)
        scrollView.contentSize = CGSize(width: framesView.frame.width + scrollViewFramesInset * 2, height: scrollView.bounds.size.height)

        scrollView.setContentOffset(.zero, animated: true)
        hideShowDotsOnSides(isDotHidden: duration < 1.05)
        enableDisableMainBordersMovement(isMovementEnabled: duration >= 1.05)
        //delegate?.updateScissorsButtonAvailability(self, duration >= 1.05)

        trimmMode = .normal
        setNewTrimmInfo()
        updateSelectedTime(withAnimation: true)
        playFromBeginning()

        DispatchQueue.global(qos: .userInteractive).async {
            for index in 0..<self.totalFramesCount {
                let time = duration / Double(self.totalFramesCount) * Double(index)
                if let image = self.createImage(fromTime: time) {
                    self.addImageToFramesView(image: image, index: index)
                }
            }
        }
    }

    private func createImage(fromTime: Double) -> UIImage? {
        guard let imageGenerator = imageGenerator else {
            return nil
        }
        let time = CMTimeMakeWithSeconds(fromTime, preferredTimescale: 1000000)

        if let cgImage = try? imageGenerator.copyCGImage(at: time, actualTime: nil) {
            return UIImage(cgImage: cgImage)
        } else {
            return nil
        }
    }

    private func addImageToFramesView(image: UIImage, index: Int) {
        DispatchQueue.main.async {
            let width = self.framesView.bounds.width / CGFloat(self.totalFramesCount)
            let imageView = UIImageView(frame: CGRect(x: width * CGFloat(index),
                                                      y: 0,
                                                      width: width,
                                                      height: self.framesView.bounds.height))
            imageView.image = image
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            self.framesView.addSubview(imageView)
        }
    }
}

//MARK: setup methods
extension TrimmVideoView {
    private func setupDefaultBorders() {
        leftBorder = TrimmVideoViewSideBorder(frame: CGRect(x: -bounds.maxX + scrollViewFramesInset + 20, y: 0, width: bounds.size.width, height: framesView.bounds.maxY), isRight: false)

        rightBorder = TrimmVideoViewSideBorder(frame: CGRect(x: bounds.maxX - scrollViewFramesInset - 20, y: 0, width: bounds.size.width, height: framesView.bounds.maxY), isRight: true)

        topBorder.frame = CGRect(x: leftBorder.frame.maxX, y: 0, width: rightBorder.frame.minX - leftBorder.frame.maxX, height: 3)
        topBorder.backgroundColor = .white
        topBorder.isUserInteractionEnabled = false

        bottomBorder.frame = CGRect(x: leftBorder.frame.maxX, y: leftBorder.frame.maxY - 3, width: rightBorder.frame.minX - leftBorder.frame.maxX, height: 3)
        bottomBorder.backgroundColor = .white
        bottomBorder.isUserInteractionEnabled = false

        addSubview(leftBorder)
        addSubview(rightBorder)
        addSubview(topBorder)
        addSubview(bottomBorder)
    }

    private func setupSlider() {
        slider.backgroundColor = .clear
        slider.isUserInteractionEnabled = true

        let sliderViewWidth: CGFloat = 20
        slider.frame = CGRect(origin: .zero, size: CGSize(width: sliderViewWidth, height: framesView.bounds.size.height))
        insertSubview(slider, aboveSubview: scrollView)

        let whiteLine = UIView(frame: CGRect(x: sliderViewWidth / 2 - 3, y: 0, width: 6, height: framesView.bounds.size.height))
        whiteLine.backgroundColor = .white

        slider.clipsToBounds = true
        hideShowSlider(isSliderHidden: true)
        slider.addSubview(whiteLine)
    }

    private func initScrollView() {
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false

        let topConstraint = NSLayoutConstraint(item: scrollView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let trailingConstraint = NSLayoutConstraint(item: scrollView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        let leadingConstraint = NSLayoutConstraint(item: scrollView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: scrollView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60)
        NSLayoutConstraint.activate([trailingConstraint, topConstraint, leadingConstraint, heightConstraint])
    }

    private func addGestureRecognizers() {
        leftBorder.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(leftBorderMoved(gesture:))))
        rightBorder.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(rightBorderMoved(gesture:))))
        slider.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(sliderMoved(gesture:))))
    }
}

//MARK: Moving borders
extension TrimmVideoView {
    @objc private func leftBorderMoved(gesture: UIPanGestureRecognizer) {
        hideShowSlider(isSliderHidden: true)
        switch gesture.state {
        case .began:
            if trimmMode != .moveSideBorders {
                trimmMode = .moveSideBorders
                delegate?.didMovedMainBorders(self)
            }
            leftBorder.isMoving = true
            leftBorder.startPoint = leftBorder.frame.minX
            playPausePlayer(isPaused: true)
        case .changed:
            let deltaX = gesture.translation(in: leftBorder).x
            var origin = CGPoint(x: leftBorder.startPoint + deltaX, y: 0)
            let originCopy = origin

            origin.x = max(origin.x, -bounds.size.width + scrollViewFramesInset + 20) //left side
            origin.x = min(origin.x, getMaxPointForLeftBorder()) // >= 1 sec
            origin.x = min(origin.x, rightBorder.frame.minX - leftBorder.bounds.size.width) //right border

            if originCopy != origin {
                leftBorder.isForceStopped = true
            } else {
                leftBorder.isForceStopped = false
            }

            leftBorder.frame = CGRect(origin: origin, size: leftBorder.frame.size)
            updateTopAndBottomBorders()
            seekPlayerTo(point: convert(CGPoint(x: leftBorder.frame.maxX - 20, y: 0), to: framesView).x)
        case .ended, .cancelled:
            leftBorder.isMoving = false
            if !rightBorder.isMoving && !slider.isMoving {
                playFromBeginning()
            }
        default:
            break
        }
    }

    @objc private func rightBorderMoved(gesture: UIPanGestureRecognizer) {
        hideShowSlider(isSliderHidden: true)
        switch gesture.state {
        case .began:
            if trimmMode != .moveSideBorders {
                trimmMode = .moveSideBorders
                delegate?.didMovedMainBorders(self)
            }
            rightBorder.isMoving = true
            rightBorder.startPoint = rightBorder.frame.minX
            playPausePlayer(isPaused: true)
        case .changed:
            let deltaX = gesture.translation(in: rightBorder).x
            var origin = CGPoint(x: rightBorder.startPoint + deltaX, y: 0)
            let originCopy = origin

            origin.x = min(origin.x, bounds.size.width - scrollViewFramesInset - 20) //right side
            origin.x = max(origin.x, getMinPointForRightBorder()) // >= 1 sec
            origin.x = max(origin.x, leftBorder.frame.maxX) //left border

            if originCopy != origin {
                rightBorder.isForceStopped = true
            } else {
                rightBorder.isForceStopped = false
            }

            rightBorder.frame = CGRect(origin: origin, size: rightBorder.frame.size)
            updateTopAndBottomBorders()
            seekPlayerTo(point: convert(CGPoint(x: rightBorder.frame.minX + 20, y: 0), to: framesView).x)
        case .ended, .cancelled:
            rightBorder.isMoving = false
            if !leftBorder.isMoving && !slider.isMoving {
                playFromBeginning()
            }
        default:
            break
        }
    }

    private func updateTopAndBottomBorders() {
        let size = CGSize(width: rightBorder.frame.minX - leftBorder.frame.maxX, height: 3)
        topBorder.frame = CGRect(origin: CGPoint(x: leftBorder.frame.maxX, y: 0), size: size)
        bottomBorder.frame = CGRect(origin: CGPoint(x: leftBorder.frame.maxX, y: leftBorder.frame.maxY - 3), size: size)

        updateSelectedTime()
        updateSliderMaskRect()
    }

    private func updateSelectedTime(withAnimation: Bool = false) {
        guard let timelabel = timeLabel else {
            return
        }
        let startPoint = leftBorder.frame.maxX - 20 - scrollViewFramesInset + scrollView.contentOffset.x
        let startTime = Double(startPoint) * secondsPerPoint

        let endPoint = rightBorder.frame.minX + 20 - scrollViewFramesInset + scrollView.contentOffset.x
        let duration = Double(endPoint - startPoint) * secondsPerPoint

        setNewTrimmInfo(startTime: startTime, endTime: startTime + duration)

        let selectedTime = String( format: "Selected: %@ sec.",
                                   arguments: [min(duration.round(to: 1), maxVideoDuration).description])
        
        if withAnimation {
            UIView.transition(with: timelabel, duration: 0.15, options: .transitionCrossDissolve, animations: {
                timelabel.textColor = .white
                timelabel.text = selectedTime
            }, completion: nil)
        } else {
            DispatchQueue.main.async {
                timelabel.textColor = .white
                timelabel.text = selectedTime
            }
        }
    }

    private func updateSelectedExcludedTime(withAnimation: Bool = false) {
        guard let timelabel = timeLabel else {
            return
        }

        let startPoint = leftFragmentBorder.center.x - scrollViewFramesInset + scrollView.contentOffset.x
        let endPoint =  rightFragmentBorder.center.x - scrollViewFramesInset + scrollView.contentOffset.x
        let duration = Double(endPoint - startPoint) * secondsPerPoint

        let selectedTime = String(format: "Selected: %@ sec.",
                                  [min(duration.round(to: 1), maxVideoDuration).description])
        if withAnimation {
            UIView.transition(with: timelabel, duration: 0.15, options: .transitionCrossDissolve, animations: {
                timelabel.textColor = .red
                timelabel.text = selectedTime
            }, completion: nil)
        } else {
            DispatchQueue.main.async {
                timelabel.textColor = .red
                timelabel.text = selectedTime
            }
        }
    }

    private func getMaxPointForLeftBorder() -> CGFloat {
        let endPoint = rightBorder.frame.minX + 20
        let srartPoint = endPoint - pointsPerSecond - leftBorder.frame.width + 20
        return srartPoint
    }

    private func getMinPointForRightBorder() -> CGFloat {
        let startPoint = leftBorder.frame.maxX - 20
        let endPoint = startPoint + pointsPerSecond - 20
        return endPoint
    }

    private func seekPlayerTo(point: CGFloat) {
        let seconds = getSecondsFor(point: point)
        seekPlayerTo(time: CMTime(seconds: seconds, preferredTimescale: 1000))
    }
}

//MARK: Move slider
extension TrimmVideoView {
    @objc private func sliderMoved(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            slider.isMoving = true
            slider.startPoint = slider.frame.minX
            playPausePlayer(isPaused: true)
        case .changed:
            let deltaX = gesture.translation(in: slider).x
            var origin = CGPoint(x: slider.startPoint + deltaX, y: 0)
            origin.x = max(origin.x, leftBorder.frame.maxX - 20 - scrollViewFramesInset + 6) //left border
            origin.x = min(origin.x, rightBorder.frame.minX + 20 - slider.frame.width / 2) //right border
            slider.frame = CGRect(origin: origin, size: slider.frame.size)
            setMaskLayerForSlider()
            
            let seconds = getSecondsFor(point: convert(CGPoint(x: slider.frame.minX + slider.frame.width / 2, y: 0), to: framesView).x)
            seekPlayerTo(time: CMTime(seconds: seconds + 0.075, preferredTimescale: 1000))
        case .ended, .cancelled:
            slider.isMoving = false
            playPausePlayer(isPaused: true, isPauseOverlayHidden: false)
        default:
            break
        }
    }

    public func setSliderPosition(for time: Double) {
        var sliderCenter: CGPoint = .zero
        sliderCenter = framesView.convert(CGPoint(x: CGFloat(time) * pointsPerSecond, y: framesView.frame.size.height / 2), to: self)
        DispatchQueue.main.async {
            self.hideShowSlider(isSliderHidden: false)
            self.slider.center = sliderCenter
            self.setMaskLayerForSlider()
        }
    }

    private func getSecondsFor(point: CGFloat) -> Double {
        return Double(point) * secondsPerPoint
    }

    private func sliderMovedToPoint(point: CGFloat) {
        seekPlayerTo(point: point)
    }

    private func updateSliderMaskRect() {
        slider.maskRectangle = CGRect(x: leftBorder.frame.maxX - 20, y: 0, width: rightBorder.frame.minX - leftBorder.frame.maxX + 20 * 2, height: framesView.frame.height)
    }

    private func setMaskLayerForSlider() {
        slider.layer.mask = nil
        let mask = CAShapeLayer()
        mask.path = UIBezierPath(roundedRect: CGRect(origin: CGPoint(x: slider.maskRectangle.origin.x - slider.frame.minX, y: 0), size: slider.maskRectangle.size), cornerRadius: 16).cgPath
        slider.layer.mask = mask
    }
}

//MARK: setup and other UI methods
extension TrimmVideoView {
    public func setup(videoModel: EditingVideoModel?, timeLabel: UILabel?, videoView: VideoView?) {
        self.videoModel = videoModel
        self.timeLabel = timeLabel
        self.videoView = videoView
        layoutIfNeeded()
        setupFrames()
        setupDefaultBorders()
        updateSliderMaskRect()
        setupSlider()
        addGestureRecognizers()
        updateSelectedTime()
        addShadows()
        addTimeObserver()
        updateInitialConditions()
    }

    private func hideShowDotsOnSides(isDotHidden: Bool) {
        leftBorder.hideShowDraggableDot(isDotHidden: isDotHidden)
        rightBorder.hideShowDraggableDot(isDotHidden: isDotHidden)
    }

    internal func hideShowSlider(isSliderHidden: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.slider.isHidden = isSliderHidden
        }
    }

    private func hideShowSelectedTimeLabel(isLabelHidden: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.timeLabel?.alpha = isLabelHidden ? 0 : 1
        }
    }

    private func addShadows() {
        for subview in subviews {
            if subview != topBorder && subview != bottomBorder {
                subview.addShadow(offset: .zero, radius: 3, color: .black, opacity: 0.3)
            }
        }
        addShadow(offset: .zero, radius: 2, color: .black, opacity: 0.3)
    }

    private func updateInitialConditions() {
        if let duration = asset?.duration.seconds {
            hideShowDotsOnSides(isDotHidden: duration < 1.05)
            //delegate?.updateScissorsButtonAvailability(self, duration > 1.05)
        }
    }

    private func updateForMode() {
        switch trimmMode {
        case .normal:
            hideShowSelectedTimeLabel(isLabelHidden: false)
            scrollView.isScrollEnabled = true
            if let totalDuration = asset?.duration.seconds {
                hideShowDotsOnSides(isDotHidden: totalDuration < 1.05)
                enableDisableMainBordersMovement(isMovementEnabled: totalDuration >= 1.05)
                //delegate?.updateScissorsButtonAvailability(self, totalDuration >= 1.05)
            }
        updateSelectedTime(withAnimation: true)
        case .moveSideBorders:
            hideShowSelectedTimeLabel(isLabelHidden: false)
            hideShowDotsOnSides(isDotHidden: false)
            scrollView.isScrollEnabled = true
            updateSelectedTime(withAnimation: true)
            enableDisableMainBordersMovement(isMovementEnabled: true)
        case .cutFragment:
            hideShowSelectedTimeLabel(isLabelHidden: false)
            hideShowDotsOnSides(isDotHidden: true)
            scrollView.isScrollEnabled = false
            addDefaultFragmentForRemoving()
            enableDisableMainBordersMovement(isMovementEnabled: false)
        }
    }

    private func enableDisableMainBordersMovement(isMovementEnabled: Bool) {
        leftBorder.isUserInteractionEnabled = isMovementEnabled
        rightBorder.isUserInteractionEnabled = isMovementEnabled
    }
}

//MARK: cut fragment functionality
extension TrimmVideoView: CAAnimationDelegate {
    private func addDefaultFragmentForRemoving() {
        playPausePlayer(isPaused: true, isPauseOverlayHidden: false)
        hideShowSlider(isSliderHidden: true)
        showFragmentBorders()
    }

    private func showFragmentBorders() {
        leftFragmentBorder.center = slider.center
        rightFragmentBorder.center = slider.center
        leftFragmentBorder.isHidden = false
        rightFragmentBorder.isHidden = false

        setMaskLayerFor(fragmentBorder: leftFragmentBorder)

        redDeleteFragment.frame = CGRect(x: leftFragmentBorder.center.x - scrollViewFramesInset, y: 0, width: 0, height: framesView.frame.height)
        redDeleteFragment.isHidden = false
        redDeleteFragment.alpha = 1
        framesView.bringSubviewToFront(redDeleteFragment)

        UIView.animate(withDuration: 0.1) { //borders appeareance in one point
            self.leftFragmentBorder.alpha = 1
            self.rightFragmentBorder.alpha = 1
        }

        let maxXForLeftFragmentBorder = rightBorder.frame.minX + 20
        var suggestedWidth = (bounds.width - scrollViewFramesInset * 2) / 10
        //if video + suggestedWidth < 1 sec
        if let videoDuration = asset?.duration.seconds {
            let availableWidthForDeleting = CGFloat(videoDuration - 1) * pointsPerSecond
            suggestedWidth = min((bounds.width - scrollViewFramesInset * 2) / 10, availableWidthForDeleting)
        }
        //if video + suggestedWidth > right border
        let actualCenterX = min(self.leftFragmentBorder.center.x + suggestedWidth, maxXForLeftFragmentBorder)

        UIView.animate(withDuration: 0.3, animations: { //move right border to <= 10% of selected range + add red area
            self.rightFragmentBorder.center = CGPoint(x: actualCenterX, y: self.leftFragmentBorder.center.y)
            self.redDeleteFragment.frame = CGRect(x: self.leftFragmentBorder.center.x - self.scrollViewFramesInset, y: 0, width: actualCenterX - self.leftFragmentBorder.center.x, height: self.redDeleteFragment.frame.height)
            self.setMaskLayerFor(fragmentBorder: self.rightFragmentBorder)
        }) { (_) in
            self.hideShowSlider(isSliderHidden: true)
            self.leftFragmentBorder.animateDotsAppearance()
            self.rightFragmentBorder.animateDotsAppearance()
            self.updateSelectedExcludedTime(withAnimation: true)
        }
    }

    private func hideFragmentBorders(isRemovingRange: Bool, confirmButtonCenter: CGPoint? = nil) {
        isUserInteractionEnabled = false
        if isRemovingRange, let basketCenter = confirmButtonCenter {

            let jumpingView = UIView(frame: convert(redDeleteFragment.frame, from: framesView))
            jumpingView.tag = 77
            jumpingView.clipsToBounds = true
            jumpingView.contentMode = .center

            if let deletedFramesView = framesView.resizableSnapshotView(from: redDeleteFragment.frame, afterScreenUpdates: true, withCapInsets:
                .zero) {
                jumpingView.addSubview(deletedFramesView)

                let redOverlay = UIView(frame: deletedFramesView.bounds)
                redOverlay.backgroundColor = UIColor.red.withAlphaComponent(0.4)
                deletedFramesView.addSubview(redOverlay)

                self.addSubview(jumpingView)

                UIView.animate(withDuration: 0.15) {
                    self.leftFragmentBorder.alpha = 0
                    self.rightFragmentBorder.alpha = 0
                    self.leftFragmentBorder.isHidden = true
                    self.rightFragmentBorder.isHidden = true
                    self.redDeleteFragment.isHidden = true
                }

                let positionAnimation = CAKeyframeAnimation(keyPath:"position")
                let path = UIBezierPath()
                path.move(to: jumpingView.center)
                path.addQuadCurve(to: basketCenter, controlPoint: CGPoint(x: bounds.maxX - scrollViewFramesInset,
                                                                          y: bounds.maxY - bounds.size.height*1.5))
                positionAnimation.path = path.cgPath

                let frameAnimation = CABasicAnimation(keyPath: "bounds.size")
                frameAnimation.fromValue = jumpingView.bounds.size
                frameAnimation.toValue = CGSize(width: 15, height: 15)

                let opacityAnination = CABasicAnimation(keyPath: "opacity")
                opacityAnination.fromValue = 1
                opacityAnination.toValue = 0.4

                let group = CAAnimationGroup()
                group.duration = 0.8
                group.animations = [positionAnimation, frameAnimation, opacityAnination]
                group.isRemovedOnCompletion = false
                group.repeatCount = 1
                group.autoreverses = false
                group.delegate = self
                group.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
                group.fillMode = CAMediaTimingFillMode.forwards

                jumpingView.layer.add(group, forKey: "groupAnimation")
            }
        } else {
            let centerX = leftFragmentBorder.center.x + (rightFragmentBorder.center.x - leftFragmentBorder.center.x)/2
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseIn, animations: {
                self.leftFragmentBorder.center = CGPoint(x: centerX, y: self.leftFragmentBorder.center.y)
                self.rightFragmentBorder.center = CGPoint(x: centerX, y: self.rightFragmentBorder.center.y)
                self.redDeleteFragment.frame = CGRect(x: centerX - self.scrollViewFramesInset, y: 0, width: 0, height: self.redDeleteFragment.frame.height)
            }) { (_) in
                UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear, animations: {
                    self.leftFragmentBorder.alpha = 0
                    self.rightFragmentBorder.alpha = 0
                    self.redDeleteFragment.isHidden = true
                }) { (_) in
                    self.leftFragmentBorder.isHidden = true
                    self.rightFragmentBorder.isHidden = true
                    self.playFromBeginning()
                    self.trimmMode = .normal
                    self.isUserInteractionEnabled = true
                }
            }
        }
    }

    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        viewWithTag(77)?.removeFromSuperview()
        trimmMode = .normal
        UIFeedbackManager.shared.generateImpact(style: .success)
    }

    @objc private func leftFragmentBorderMoved(gesture: UIPanGestureRecognizer) {
        guard trimmMode == .cutFragment else {
            return
        }
        hideShowSlider(isSliderHidden: true)
        switch gesture.state {
        case .began:
            leftFragmentBorder.isMoving = true
            leftFragmentBorder.startPoint = leftFragmentBorder.center.x
            playPausePlayer(isPaused: true)
        case .changed:
            let deltaX = gesture.translation(in: leftFragmentBorder).x
            var newCenter = CGPoint(x: leftFragmentBorder.startPoint + deltaX, y: leftFragmentBorder.center.y)
            let newCenterCopy = newCenter

            newCenter.x = max(newCenter.x, getMinXForLeftFragmentBorder())
            newCenter.x = min(newCenter.x, rightFragmentBorder.center.x)

            if newCenterCopy != newCenter {
                leftFragmentBorder.isForceStopped = true
            } else {
                leftFragmentBorder.isForceStopped = false
            }

            leftFragmentBorder.center = newCenter
            updateRedViewFrame()
            setMaskLayerFor(fragmentBorder: leftFragmentBorder)
            updateSelectedExcludedTime()
            seekPlayerTo(point: self.convert(CGPoint(x: newCenter.x , y: 0), to: framesView).x)
        case .ended, .cancelled:
            leftFragmentBorder.isMoving = false
            if !rightFragmentBorder.isMoving {
                playPausePlayer(isPaused: true, isPauseOverlayHidden: false)
            }
        default:
            break
        }
    }

    @objc private func rightFragmentBorderMoved(gesture: UIPanGestureRecognizer) {
        guard trimmMode == .cutFragment else {
            return
        }
        hideShowSlider(isSliderHidden: true)
        switch gesture.state {
        case .began:
            rightFragmentBorder.isMoving = true
            rightFragmentBorder.startPoint = rightFragmentBorder.center.x
            playPausePlayer(isPaused: true)
        case .changed:
            let deltaX = gesture.translation(in: rightFragmentBorder).x
            var newCenter = CGPoint(x: rightFragmentBorder.startPoint + deltaX, y: rightFragmentBorder.center.y)
            let newCenterCopy = newCenter

            newCenter.x = max(newCenter.x, leftFragmentBorder.center.x)
            newCenter.x = min(newCenter.x, getMaxXForRightFragmentBorder())

            if newCenterCopy != newCenter {
                rightFragmentBorder.isForceStopped = true
            } else {
                rightFragmentBorder.isForceStopped = false
            }

            rightFragmentBorder.center = newCenter
            updateRedViewFrame()
            setMaskLayerFor(fragmentBorder: rightFragmentBorder)
            updateSelectedExcludedTime()
            seekPlayerTo(point: self.convert(CGPoint(x: newCenter.x, y: 0), to: framesView).x)
        case .ended, .cancelled:
            rightFragmentBorder.isMoving = false
            if !leftFragmentBorder.isMoving {
                playPausePlayer(isPaused: true, isPauseOverlayHidden: false)
            }
        default:
            break
        }
    }

    //returns x where (start -> left side) + (right side -> end) >= 1 sec
    private func getMinXForLeftFragmentBorder() -> CGFloat {
        var minLeftPoint = leftBorder.frame.maxX - 20

        let endPoint = rightBorder.frame.minX + 20
        let rightBorderCenter = rightFragmentBorder.center.x
        let rightTime = Double(endPoint - rightBorderCenter) * secondsPerPoint

        if rightTime < 1 {
            minLeftPoint += CGFloat(1 - rightTime) * pointsPerSecond
        }

        return minLeftPoint
    }

    //returns x where (start -> left side) + (right side -> end) >= 1 sec
    private func getMaxXForRightFragmentBorder() -> CGFloat {
        var maxRightPoint = rightBorder.frame.minX + 20

        let startPoint = leftBorder.frame.maxX - 20
        let leftBorderCenter = leftFragmentBorder.center.x
        let leftTime = Double(leftBorderCenter - startPoint) * secondsPerPoint

        if leftTime < 1 {
            maxRightPoint -= CGFloat(1 - leftTime) * pointsPerSecond
        }

        return maxRightPoint
    }


    private func setMaskLayerFor(fragmentBorder: TrimmVideoViewFragmentBorder) {
        fragmentBorder.layer.mask = nil
        let mask = CAShapeLayer()
        mask.path = UIBezierPath(roundedRect: CGRect(origin: CGPoint(x: slider.maskRectangle.origin.x - fragmentBorder.frame.minX, y: 0), size: slider.maskRectangle.size), cornerRadius: 16).cgPath
        fragmentBorder.layer.mask = mask

        setMaskLayerForColorSegment(segment: redDeleteFragment)
    }

    private func setMaskLayerForColorSegment(segment: UIView) {
        segment.layer.mask = nil
        let mask = CAShapeLayer()
        mask.path = UIBezierPath(roundedRect: CGRect(origin: CGPoint(x: slider.maskRectangle.origin.x - segment.frame.minX - scrollViewFramesInset, y: 0), size: slider.maskRectangle.size), cornerRadius: 16).cgPath
        segment.layer.mask = mask
    }

    private func updateRedViewFrame() {
        self.redDeleteFragment.frame = CGRect(x: self.leftFragmentBorder.center.x - self.scrollViewFramesInset, y: 0, width: self.rightFragmentBorder.center.x - self.leftFragmentBorder.center.x, height: self.redDeleteFragment.frame.height)
    }
}

//MARK: CONFIRM MODES ACTIONS
extension TrimmVideoView {
    public func undoAction() {
        playPausePlayer(isPaused: true)
        hideShowSlider(isSliderHidden: true)

        switch trimmMode {
        case .moveSideBorders:
            moveMainBordersToDefaultPosition(isDeleting: false)
        case .cutFragment:
            trimmMode = .normal
            hideFragmentBorders(isRemovingRange: false)
        default:
            break
        }
    }

    public func confirmAction(confirmButtonCenter: CGPoint? = nil) {
        playPausePlayer(isPaused: true)
        hideShowSlider(isSliderHidden: true)

        switch trimmMode {
        case .moveSideBorders: //deleting side ranges
            confirmBordersMoving()
        case .cutFragment: //deleting selected fragment
            confirmFragmentRemoving(confirmButtonCenter: confirmButtonCenter)
        default:
            break
        }
    }
    
    public func confirmFragmentRemoving(confirmButtonCenter: CGPoint? = nil) {
        let startPoint = leftFragmentBorder.center.x - scrollViewFramesInset + scrollView.contentOffset.x
        let endPoint = rightFragmentBorder.center.x - scrollViewFramesInset + scrollView.contentOffset.x

        let startTime = getSecondsFor(point: startPoint)
        let endTime = getSecondsFor(point: endPoint)

        let duration = Double(endPoint - startPoint) * secondsPerPoint

        if duration > 0 {
            let excludedTimeRange = CMTimeRange(start: CMTime(seconds: startTime, preferredTimescale: 1000),
                                                end: CMTime(seconds: endTime, preferredTimescale: 1000))
            delegate?.didRemoveTimeRange(self, timeRange: excludedTimeRange)
            delegate?.didFinishEditing(self, true)
        }

        hideFragmentBorders(isRemovingRange: duration > 0, confirmButtonCenter: confirmButtonCenter)
    }

    public func confirmBordersMoving() {
        if leftBorder.frame.maxX - 20 != scrollViewFramesInset || rightBorder.frame.minX + 20 != bounds.width - scrollViewFramesInset || scrollView.contentOffset.x > 0 {
            updateAvailableTimeRanges()
            moveMainBordersToDefaultPosition(isDeleting: true)
        } else {
            delegate?.didFinishEditing(self, false)
            trimmMode = .normal
            setNewTrimmInfo()
            playFromBeginning()
        }
    }

    private func moveMainBordersToDefaultPosition(isDeleting: Bool) {
        let leftBorderOriginalX = -leftBorder.frame.width + scrollViewFramesInset + 20
        let leftBorderOriginalFrame = CGRect(x: leftBorderOriginalX, y: 0, width: leftBorder.frame.width, height: leftBorder.frame.height)

        let rightBorderOriginalX = bounds.width - scrollViewFramesInset - 20
        let rightBorderOriginalFrame = CGRect(x: rightBorderOriginalX, y: 0, width: rightBorder.frame.width, height: rightBorder.frame.height)

        UIView.animate(withDuration: 0.3, animations: {
            self.leftBorder.frame = leftBorderOriginalFrame
            self.rightBorder.frame = rightBorderOriginalFrame

            self.topBorder.frame = CGRect(origin: CGPoint(x: self.scrollViewFramesInset + 20, y: 0), size: CGSize(width: self.bounds.width - self.scrollViewFramesInset * 2 - 20 * 2, height: self.topBorder.frame.height))
            self.bottomBorder.frame = CGRect(origin: CGPoint(x: self.scrollViewFramesInset + 20, y: self.leftBorder.frame.maxY - 3), size: CGSize(width: self.bounds.width - self.scrollViewFramesInset * 2 - 20 * 2, height: self.topBorder.frame.height))
        }) { (_) in
            self.setNewTrimmInfo()
            self.updateSelectedTime(withAnimation: true)
            self.updateSliderMaskRect()
            self.trimmMode = .normal
            if !isDeleting {
                self.playFromBeginning()
            }
        }
    }

    private func updateAvailableTimeRanges() {
        guard let videoModel = videoModel else {
            return
        }

        let leftRange = CMTimeRange(start: CMTime(seconds: .zero, preferredTimescale: 1000),
                                    end: CMTime(seconds: videoModel.trimVideoInfo.startTime, preferredTimescale: 1000))
        let rightRange = CMTimeRange(start: CMTime(seconds: videoModel.trimVideoInfo.endTime, preferredTimescale: 1000),
                                     end: CMTime(seconds: asset?.duration.seconds ?? maxVideoDuration, preferredTimescale: 1000))

        if leftRange.duration.seconds > 5/1000 {
            delegate?.didRemoveTimeRange(self, timeRange: leftRange)
        }
        if rightRange.duration.seconds > 5/1000 {
            delegate?.didRemoveTimeRange(self, timeRange: rightRange)
        }

        delegate?.didFinishEditing(self, true)
        setNewTrimmInfo()
    }

    private func setNewTrimmInfo(startTime: Double? = nil, endTime: Double? = nil) {
        guard let videoModel = videoModel else {
            return
        }

        if let startTime = startTime, let endTime = endTime {
            videoModel.trimVideoInfo.startTime = startTime
            videoModel.trimVideoInfo.endTime = endTime
        } else if let duration = asset?.duration.seconds {
            videoModel.trimVideoInfo.startTime = .zero
            videoModel.trimVideoInfo.endTime = min(maxVideoDuration, duration)
        }
    }
}

//MARK: UIScrollViewDelegate methods
extension TrimmVideoView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateSelectedTime()
        if let startTime = videoModel?.trimVideoInfo.startTime {
            seekPlayerTo(time: CMTime(seconds: startTime, preferredTimescale: 1000))
        }
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        playPausePlayer(isPaused: true)
        hideShowSlider(isSliderHidden: true)
        trimmMode = .moveSideBorders
        delegate?.didMovedMainBorders(self)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if !scrollView.isDragging {
            playFromBeginning()
        }
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !scrollView.isScrolling {
            playFromBeginning()
        }
    }
}
