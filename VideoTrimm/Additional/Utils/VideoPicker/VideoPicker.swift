//
//  VideoPicker.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import UIKit
import PhotosUI

class VideoPicker: NSObject {
    enum SourceType {
        case camera
        case gallery
    }
    
    private lazy var imagePickerController: UIImagePickerController = {
        let controller = UIImagePickerController()
        
        controller.delegate = self
        controller.mediaTypes = ["public.movie"]
        
        return controller
    }()
    
    private weak var presentationController: UIViewController?
    
    public weak var delegate: VideoPickerDelegate?
}

//MARK: -Private methods
private extension VideoPicker {
    func videoPicker(_ controller: UIViewController?, didSelect videoURL: URL?) {
        controller?.dismiss(animated: true, completion: nil)
        
        if let videoURL = videoURL {
            delegate?.didSelect(videoURL: videoURL)
        }
    }
}

//MARK: -Public methods
extension VideoPicker {
    public func present(for type: SourceType, above controller: UIViewController) {
        presentationController = controller
        
        switch type {
        case .camera:
            if AVCaptureDevice.authorizationStatus(for: .video) == .denied {
                delegate?.failedWithDeniedCameraPermission()
                return
            } else {
                imagePickerController.sourceType = .camera
            }
        case .gallery:
            imagePickerController.sourceType = .photoLibrary
        }
        
        imagePickerController.modalPresentationStyle = .overFullScreen
        presentationController?.present(imagePickerController, animated: true)
    }
}

//MARK: -UIImagePickerControllerDelegate
extension VideoPicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        videoPicker(picker, didSelect: nil)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController,
                                        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        let videoURL = info[.mediaURL] as? URL
        videoPicker(picker, didSelect: videoURL)
    }
}
