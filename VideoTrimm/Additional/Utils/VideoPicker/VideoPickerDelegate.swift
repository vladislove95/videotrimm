//
//  VideoPicker.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import Foundation

protocol VideoPickerDelegate: class {
    func didSelect(videoURL: URL)
    func failedWithDeniedCameraPermission()
}
