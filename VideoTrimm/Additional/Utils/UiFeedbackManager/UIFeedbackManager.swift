//
//  Extensions.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import Foundation
import UIKit

enum UIFeedbackStyle {
    /// UIImpactFeedbackGenerator styles. Use impact feedback styles to indicate that an impact has occurred. For example, you might trigger impact feedback when a user interface object collides with something or snaps into place.
    case light
    case medium
    case heavy
    case soft
    case rigid
    /// UINotificationFeedbackGenerator styles. Use notification feedback generators to indicate successes, failures, and warnings.
    case success
    case warning
    case error
    /// UISelectionFeedbackGenerator style. Use this selection feedback style to indicate a change in selection.
    case selectionChanged
}

/**
Generates any kind of feedback impact. Check all styles on  https://developer.apple.com/design/human-interface-guidelines/ios/user-interaction/haptics/

*/
class UIFeedbackManager: NSObject {
    public static let shared = UIFeedbackManager()
    
    private lazy var impactFeedbackGenerator = {
        return UIImpactFeedbackGenerator()
    }()
    
    private lazy var notificationFeedbackGenerator = {
        return UINotificationFeedbackGenerator()
    }()
    
    private lazy var selectionFeedbackGenerator = {
        return UISelectionFeedbackGenerator()
    }()

    private var currentStyle: UIFeedbackStyle = .light {
        didSet {
            if oldValue != currentStyle {
                updateGenerator(for: currentStyle)
            }
        }
    }
}


extension UIFeedbackManager {
    public func generateImpact(style: UIFeedbackStyle = .light) {
        currentStyle = style
        switch style {
        case .light, .medium, .heavy, .soft, .rigid:
            impactFeedbackGenerator.prepare()
            impactFeedbackGenerator.impactOccurred()
        case .success, .warning, .error:
            impactFeedbackGenerator.prepare()
            notificationFeedbackGenerator.notificationOccurred(getNotificationType(for: style))
        case .selectionChanged:
            impactFeedbackGenerator.prepare()
            selectionFeedbackGenerator.selectionChanged()
        }
    }
    
    private func updateGenerator(for style: UIFeedbackStyle) {
        switch style {
        case .light, .medium, .heavy, .soft, .rigid:
            impactFeedbackGenerator = UIImpactFeedbackGenerator(style: getImpactStyle(for: style))
        default:
            break
        }
    }
    
    private func getImpactStyle(for style: UIFeedbackStyle) -> UIImpactFeedbackGenerator.FeedbackStyle {
        switch style {
        case .light:
            return .light
        case .medium:
            return .medium
        case .heavy:
            return .heavy
        case .soft:
            return .soft
        case .rigid:
            return .rigid
        default:
            return .light
        }
    }
    
    private func getNotificationType(for style: UIFeedbackStyle) -> UINotificationFeedbackGenerator.FeedbackType {
        switch style {
        case .success:
            return .success
        case .warning:
            return .warning
        case .error:
            return .error
        default:
            return .success
        }
    }
}
