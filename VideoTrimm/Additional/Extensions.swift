//
//  Extensions.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import Foundation
import UIKit
import AVKit

extension UIView {
    func addShadow(offset : CGSize = .zero, radius : CGFloat = 3, color : UIColor = UIColor.black, opacity : Float = 0.5) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
    }
}

extension CGRect {
    var mid: CGPoint {
        return CGPoint(x: midX, y: midY)
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

extension UIScrollView {
    var isScrolling: Bool {
        return isDragging || isDecelerating
    }
}

extension Double {
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
}

extension UIColor {
    class var blueButton: UIColor? {
        return UIColor(red: 6/255, green: 170/255, blue: 252/255, alpha: 1)
    }
}

extension TrimmVideoView {
    internal func playPausePlayer(isPaused: Bool, isPauseOverlayHidden: Bool = true) {
        isPaused ? videoView?.pause(showPauseOverlay: !isPauseOverlayHidden) : videoView?.play()
    }

    public func playFromBeginning(isPaused: Bool = false) {
        let startTime = CMTime(seconds: videoModel?.trimVideoInfo.startTime ?? .zero, preferredTimescale: 1000)
        seekPlayerTo(time: startTime)
        if isPaused {
            playPausePlayer(isPaused: true, isPauseOverlayHidden: false)
            setSliderPosition(for: startTime.seconds)
        } else {
            playPausePlayer(isPaused: isPaused)
        }
    }

    internal func seekPlayerTo(time: CMTime, continuePlaying: Bool = false, completion: (() -> Void)? = nil) {
        guard let player = self.videoView?.player, player.status == .readyToPlay else {
            return
        }
        player.seek(to: time, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero) { (_) in
            completion?()
        }
    }

    private func removeObserverAndSeekTo(time: CMTime, continuePlaying: Bool) {
        guard let videoView = videoView else {
            return
        }
        if let timeObserver = videoView.playerTimeObserver {
            videoView.player?.removeTimeObserver(timeObserver)
            videoView.playerTimeObserver = nil
        }
        self.seekPlayerTo(time: time, continuePlaying: continuePlaying, completion: { [weak self] in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                self.addTimeObserver()
            }
        })
    }

    internal func addTimeObserver() {
        guard let videoModel = videoModel, let videoView = videoView else {
            return
        }

        if let ob = videoView.playerTimeObserver {
            videoView.player?.removeTimeObserver(ob)
        }

        videoView.playerTimeObserver = videoView.player?.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 60), queue: DispatchQueue.main) { [weak self] time in
            guard let self = self, self.videoView?.player?.rate == 1 else {
                return
            }

            switch self.trimmMode {
            case .normal:
                let endTime = videoModel.trimVideoInfo.endTime
                if time.seconds > endTime {
                    self.removeObserverAndSeekTo(time: CMTime.zero, continuePlaying: true)
                    self.setSliderPosition(for: CMTime.zero.seconds)
                } else {
                    self.setSliderPosition(for: time.seconds)
                }
            case .moveSideBorders:
                let startTime = videoModel.trimVideoInfo.startTime
                let endTime = videoModel.trimVideoInfo.endTime
                if time.seconds > endTime {
                    let beginning = CMTime(seconds: startTime + 0.075, preferredTimescale: 1000)
                    self.removeObserverAndSeekTo(time: beginning, continuePlaying: true)
                    self.setSliderPosition(for: startTime)
                } else {
                    self.setSliderPosition(for: time.seconds)
                }
            case .cutFragment:
                let endTime = videoModel.trimVideoInfo.endTime
                if time.seconds > endTime {
                    let newStartTime = self.removingFragmentRange.start.seconds == .zero ? (self.removingFragmentRange.end.seconds + 0.08) : .zero
                    self.removeObserverAndSeekTo(time: CMTime(seconds: newStartTime, preferredTimescale: 1000), continuePlaying: true)
                } else if time.seconds > self.removingFragmentRange.start.seconds && time.seconds < self.removingFragmentRange.end
                .seconds { //not in removing range
                    let endOfRange = CMTime(seconds: self.removingFragmentRange.end.seconds + 0.08, preferredTimescale: 1000)
                    self.removeObserverAndSeekTo(time: endOfRange, continuePlaying: true)
                    self.hideShowSlider(isSliderHidden: true)
                } else {
                    self.setSliderPosition(for: time.seconds)
                }
            }
        }
    }
}

