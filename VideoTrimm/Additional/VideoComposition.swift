//
//  VideoComposition.swift
//  VideoTrimm
//
//  Created by Vlad on 03.04.2021.
//

import Foundation
import AVKit

fileprivate let originalAudioTrackID = CMPersistentTrackID(18213)

class VideoComposition {
    var deletedRanges: [CMTimeRange] = []
    var playerItem: AVPlayerItem?
    
    func copy() -> VideoComposition {
        let new = VideoComposition()
        new.deletedRanges = deletedRanges
        return new
    }
    
    func setup(player: AVPlayer) {
        guard let asset = player.currentItem?.asset else { return }
        let composition = AVMutableComposition()
        
        if let vid = asset.tracks(withMediaType: .video).first {
            let track = composition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
            ((try? track?.insertTimeRange(vid.timeRange, of: vid, at: CMTime.zero)) as ()??)
            track?.preferredTransform = vid.preferredTransform
        }
        
        asset.tracks(withMediaType: .audio).forEach { (aud) in
            let track = composition.addMutableTrack(withMediaType: .audio, preferredTrackID: originalAudioTrackID)
            ((try? track?.insertTimeRange(aud.timeRange, of: aud, at: CMTime.zero)) as ()??)
        }
        
        let item = AVPlayerItem(asset: composition)
        player.replaceCurrentItem(with: item)
        playerItem = item
    }
    
    func updatePlayer(player: AVPlayer, fromBeginning: Bool = false) {
        guard let asset = player.currentItem?.asset else { return }
        let currentTime = player.currentTime()
        let composition = AVMutableComposition()

        if let vid = asset.tracks(withMediaType: .video).first {
            let track = composition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
            ((try? track?.insertTimeRange(vid.timeRange, of: vid, at: CMTime.zero)) as ()??)
            track?.preferredTransform = vid.preferredTransform
            deletedRanges.reversed().forEach { (range) in
                track?.removeTimeRange(range)
            }
        }

        asset.tracks(withMediaType: .audio).forEach { (aud) in
            if aud.trackID == originalAudioTrackID {
                let track = composition.addMutableTrack(withMediaType: .audio, preferredTrackID: originalAudioTrackID)
                ((try? track?.insertTimeRange(aud.timeRange, of: aud, at: CMTime.zero)) as ()??)
                deletedRanges.reversed().forEach { (range) in
                    track?.removeTimeRange(range)
                }
            }
        }
        
        let item = AVPlayerItem(asset: composition)
        player.replaceCurrentItem(with: item)
        
        if !fromBeginning {
            player.currentItem?.seek(to: currentTime, toleranceBefore: .zero, toleranceAfter: .zero, completionHandler: nil)
        }
        
        playerItem = item
        deletedRanges.removeAll()
    }
}
